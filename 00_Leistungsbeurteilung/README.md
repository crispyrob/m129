# Leistungsbeurteilung Modul 129 (V1)

Die Schlussnote setzt sich wie folgt zusammen:
```
N = LB1*0.3 + LB2*0.7
```

# LB1 - Schriftliche Prüfung

|   |   |
|---|---|
| Dauer  | 1 Lektion  |
| Erlaubte Hilfsmittel | Selbstgeschriebene Zusammenfassung (siehe Hinweise) |
| Zulassungsbedingung | Keine |
| Elektronische Hilfsmittel | Nicht erlaubt |

Der Prüfungsinhalt umfasst alle bis dahin besprochenen
 Unterlagen. Der definitive Prüfungsinhalt wird zwei Wochen vorher bekanntgegeben. 

## Persönliche Zusammenfassung
 - Umfang:
   - Ausgedruckt: Max. 2 x A4 Papier (=4 Seiten)
   - Handgeschrieben: Max 4 x A4 Papier (=8 Seiten)
 - Individuelle und selbstgeschriebene Zusammenfassung
 - Gruppenarbeiten: Maximal 2 Personen
 - Keine „Collagen“: Aus den Unterlagen 1:1 übernommenen Text oder
Seitenausschnitte sind nicht zugelassen.  
 - Ausschnitte von einzelnen Grafiken / Schemas ist erlaubt.
 - Zusammenfassung muss mit der Prüfung abgegeben werden!
 - Nichterfüllen eines Kriteriums: 1 Note Abzug oder Ausschluss aus der Prüfung

# LB2 - Praktische Übungen

**Neuer Bewertungsraster:** Die Kriterien werden im Laufe des Moduls angepasst. 

## Kriterien (Arbeitsaufträge / Kompetenzen)
| N°   | Kriterium                                                                                                                                         | Max  |
|------|---------------------------------------------------------------------------------------------------------------------------------------------------|------|
| 1    | BeASwitch: 30/30 richtig<br/>Pro Fehler 0.1 Abzug                                                                                                 | 1 P  |
| 2    | Mündliche Prüfung Switching<br/>Sie erklären auf einen Papier wie Switching funktioniert und beantworten alle Fragen der Lehrperson korrekt.      | 1 P  |
| 3    | BeARouter: 50/50 richtig<br/>Pro Fehler 0.1 Abzug                                                                                                 | 1 P  |
| 4    | Mündliche Prüfung IPv4 Subnetting<br/>Sie erklären auf einen Papier wie Routing funktioniert und beantworten alle Fragen der Lehrperson korrekt.  | 1 P  |
| 5.1  | GNS3 Labor 1: Ping mit Switch                                                                                                                     | 1 P  |
| 5.2  | Dokumentation zu Labor 1                                                                                                                          | 1 P  |
| 6.1  | GNS3 Labor 2: Ping mit Router                                                                                                                     | 1 P  |
| 6.2  | Dokumentation zu Labor 2                                                                                                                          | 1 P  |
| 7.1  | GNS3 Labor 3: Ping über Router (3 Subnetze) und lokalem PC                                                                                        | 1 P  |
| 7.2  | Dokumentation zu Labor 3                                                                                                                          | 1 P  |
| 8.1  | GNS3 Labor 4: Aggregierte statische Routen | 1 P  |
| 8.2  | Dokumentation zu Labor 4                                                                                                                          | 1 P  |
| 9.1  | GNS3 Labor 5: Labor mit zwei Router und DHCP Server                                                                                               | 1 P  |
| 9.2  | Dokumentation zu Labor 5                                                                                                                          | 1 P  |
| 10.1 | GNS3 Labor 6: Labor mit zwei Router (NAT ins Internet), Debian Client                                                                             | 1 P  |
| 10.2 | Dokumentation zu Labor 6                                                                                                                          | 1 P  |
| 11.1 | GNS3 Labor 7: Erweiterung Labor 6 mit Firewall                                                                                                    | 1 P  |
| 11.2 | Dokumentation zu Labor 7                                                                                                                          | 1 P  |
| 12.1 | GNS3 Labor 8: Ring mit 3 Routern, OSPF, DHCP und NAT ins Internet                                                                                 | 1 P  |
| 12.2 | Dokumentation zu Labor 8                                                                                                                          | 1 P  |
| 13   | *Extrapunkt:* Dokumentation in Markdown in einem Repository auf GitLab / GitHub geführt                                                                         | 1 P  |
|      | **Total** | **21 P** |

Notenberechnung: 
```
Erreichte Punktzahl / Totale Punktzahl * 5 + 1
```

## Abgabemodalitäten
 - Sie können jedes Kriterium (Arbeitsauftrag) jederzeit Abgeben. Vereinbaren Sie mit der Lehrpersonen einen Abgabetermin. 
 - Kümmern Sie sich möglichst früh um einen Abgabetermin. Termine gegen Ende des Moduls sind schnell ausgebucht.
 - Kriterien 1 und 2 müssen vor den Weihnachtsferien (17.01.2021, 16:40) abgegeben haben.
 - Spätestens bis am Fr 28.01.2021 23:59 müssen Kriterien 3 bis 10.2 abgegeben werden.
 - **Sie müssen sich selbstständig um die Termine kümmern!**

## Mündliche Prüfungen
Die mündliche Prüfung gilt als Bestanden, wenn Sie die Fragen der Lehrperson Kompetent beantworten können. 

### Thema Switching
Sie erklären an der Wandtafel oder mit Papier und Stift:
- Der Vorgang, wenn ein Host ein Ethernet Frame an einen Switch schickt
- Die Funktionsweise der MAC Tabelle
- Unterschied zwischen einem HUB und einem Switch

Zusätzlich stellt Ihnen die Lehrperson fragen, um zu Prüfen ob das Thema wirklich verstanden haben. 
(Beispiel: Was passiert, wenn ein Host den Port wechselt?)

Hilfsmittel: Keine

### Thema Routing / IPv4
Die Lehrperson stellt Ihnen Fragen. Beispiele:
 - Wie viele Hosts können in einem /26 Netzwerk adressiert werden?
 - Ist die IP Adresse 192.168.23.68 mit der Subnetzmaske 255.255.255.252 eine gültige Host-Adresse? Wenn nicht, was ist Sie dann?
 - Darf ich die IPv4 Adresse 192.167.1.1/24 in meinem privaten LAN verwenden? Erklären Sie. 

Hilfsmittel: IPv4 Rechner, Wikipedia Artikel zu IPv4 (Laptop)

## BeASwitch / BeARouter
Folgt

## Mindestanforderung an die Dokumentation
Erwartet wird eine Dokumentation im Umfang dieses Beispiels [Beispiel Dokumentation.md](../20_GNS3%20Einführung/Beispiel%20Dokumentation.md)

Das Kriterium *Dokumentation des GNS3 Labors X* ist dann erfüllt, 
- wenn eine Fachperson mithilfe ihrer Dokumentation das Labor ohne Rückfragen nachbauen kann.
- wenn eine kurze Reflexion die neuen Lerninhalte und Herausforderungen auflistet. 
- wenn die Dokumentation zum Zeitpunkt der Abgabe komplett vorhanden ist. 

## Abgabe der Dokumentation
Sie haben zwei Möglichkeiten zur Abgabe der Dokumentation
 - **Empfohlen:** Ein privates GitLab Repository. Textdokumente in Markdown Syntax. Siehe [Infos zu Markdown und Git](../04_MarkdownGit/README.md)
 - Alternativ: OneNote Klassennotebook

