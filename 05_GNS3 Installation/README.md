# M129 - GNS3 Lab - Installation <!-- omit in toc -->

Im Rahmen des Moduls 145 werden Sie verschiedene neue Netzwerktechnologien kennenlernen. Die dazugehörigen praktischen Übungen werden Sie (fast) alle mit GNS3 (Graphical Network Simulator-3) durchführen. GNS3 ist ein leistungsstarker und umfangreicher Netzwerk Emulator, der es erlaubt echte Networking-Software auszuführen. Vielleicht haben Sie bereits mit VMWare, VirtualBox oder Hyper-V gearbeitet. Diese Produkte erlauben es Ihnen Betriebssysteme wie Microsoft Windows, Linux oder sogar Mac OS X in einer virtuellen Maschine auf ihrem Computer auszuführen. GNS3 lässt Sie dasselbe tun mit Cisco IOS, Juniper Routers, MikroTik und einigen weiteren Herstellern. Zusätzlich hat es ein grafisches Frontend mit dem Sie die emulierte Netzwerktopologie erstellen und visualisieren können.

Die Leistungsfähigkeit von GNS3 zeigt sich dadurch, dass es u.a. an Hochschulen und Netzwerk-Zertifizierungskurse (z.B. CCNA) eingesetzt wird. Es erlaubt Ihnen auch komplexe Routing, VPN Szenarien oder sogar Systeme mit OpenFlow zu simulieren. Wenn zum ersten Mal mit einer Netzwerktechnologie (z.B. OSPF) arbeiten und Ihr Konzept überprüfen wollen, dann ist GNS3 genau das richtige Tool.

![](media/1d36f9957c84bab58e0427f5e16c075d.png)

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. GNS3](#1-gns3)
  - [1.1. Projektverwaltung](#11-projektverwaltung)
  - [1.2. Beschaffung von Informationen](#12-beschaffung-von-informationen)
  - [1.3. Tipps](#13-tipps)
- [2. Installation](#2-installation)
  - [2.1. OpenVPN](#21-openvpn)
  - [2.2. GNS3 Client](#22-gns3-client)
  - [2.3. Verbindungstest](#23-verbindungstest)
  - [2.4. GNS3 GUI](#24-gns3-gui)
- [3. Hinweise](#3-hinweise)
  - [3.1. Mehrere Projekte auf dem gleichen Node](#31-mehrere-projekte-auf-dem-gleichen-node)
    - [3.1.1. IP-Konflikte vermeiden](#311-ip-konflikte-vermeiden)
    - [3.1.2. Unterschiedliche Projektnamen](#312-unterschiedliche-projektnamen)

# 1. GNS3

## 1.1. Projektverwaltung

Für die Projektverwaltung innerhalb von GNS3 gilt es folgendes zu beachten:

-   Erstellen Sie für jede Übung ein eigenes GNS3 Projekt.
-   Geben Sie dem Projekt einen sinnvollen Namen (z.B. Gruppennummer und Übungsnummer).
-   Entladen Sie Projekte, die Sie gerade nicht verwenden.

## 1.2. Beschaffung von Informationen

Es wird von Ihnen erwartet, dass Sie selbstständig im Internet und in den Unterlagen nach Informationen suchen. Wenn Sie nicht weiterkommen und alle Rechercheversuche erfolglos waren, **zögern Sie nicht und fragen Sie die Lehrperson**.

## 1.3. Tipps
 - Wenn Sie ein Passwort setzen müssen wählen Sie wenn immer möglich *admin* . Wenn das Gerät ein sicheres Kennwort fordert empfehle ich *Hallo1234.* oder *Passw0rd!*
 - Folgende Default Credentials benötigen Sie:
   - Debian: *debian*

# 2. Installation

Da GNS3 jedes einzelne Netzwerkgerät emuliert, beziehungsweise eine virtuelle Maschine startet, benötigt das viel Leistung. Deshalb steht für jede Gruppe ein dezidierter GNS3 Server bereit. Beachten Sie dabei folgendes:

-   Der Server hat nur begrenzt Leistung.
-   Starten Sie **nicht** alle Netzwerkgeräte auf einmal, um die Festplatte nicht auszulasten.
-   Emulieren Sie **nicht** mehr als 6 bis 7 Netzwerkgeräte pro Projekt.
-   Achten Sie auf die Auslastungsangaben im GNS3.

Natürlich dürfen Sie sich zusätzlich einen eigenen GNS3 Server auf Ihrem Laptop oder einem anderen Server aufsetzen. In der [GNS3 Dokumentation](https://docs.gns3.com/) finden Sie dafür reichlich Anleitungen. Für die Gruppenübungen empfehle ich Ihnen, den zur Verfügung gestellten Server zu verwenden.




## 2.1. OpenVPN

Die Verbindung zum GNS3 Server erfolgt über ein Layer 2 OpenVPN Tunnel.

Führen Sie diese Schritte aus, sofern Sie den OpenVPN Community Client noch nicht installiert haben.

Das Video ist ergänzend zur Anleitung und nebst der Installation auch noch einen ping-Test. 

![OpenVPN Installation Video](videos/OpenVPN_Installation.webm)

| N°| Schritt | Screenshot |
|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------|
| 1 | Laden Sie den OpenVPN Community Client auf  <https://openvpn.net/community-downloads/> herunter.                                                                                                                                                                                                                         | ![](media/643edf407ae9a374f0119ffc059bb1f2.png)  |
| 2 | Führen Sie die Installation mit den Standarteinstellungen durch.                                                                                                                                                                                                                                                         |                                                  |
| 3 | Von der Lehrperson erhalten Sie eine **ovpn** Datei.  Jede Gruppe hat jeweils **eine** *ovpn* Datei. Alle Gruppenmitglieder haben **dieselbe** Datei.  Wenn Sie die Datei noch nicht haben, melden Sie sich bei der Lehrperson.                                                                                          |                                                  |
| 4 | Erstellen Sie im Pfad  C:\\Users\\*YOUR USERNAME*\\OpenVPN\\config  einen Ordner «gns3lab» und legen Sie die *ovpn* Datei dort ab.  (Der Dateiname ist je nach Server leicht unterschiedlich.)                                                                                                                           | ![](media/d251687d8620426c0e13e53aa805e898.png)  |
| 5 | In der Taskleiste finden Sie das OpenVPN  Symbol (![](media/bff87c4dbcc1aac6a8a55b4627fc6800.png)). Klicken Sie mit der rechten Maustaste auf das Symbol und wählen Sie «Verbinden».  *Hinweis: Wenn Sie bereits OpenVPN auf Ihrem Computer installiert haben, hat es möglicherweise mehrere Verbindungen zur Auswahl.*  | ![](media/2f5c656186c63946ced240b44ba5a85a.png)  |
| 6 | Ein Statusfenster mit einem Log öffnet sich. Sobald Sie verbunden sind, schliesst sich das Fenster.                                                                                                                                                                                                                      | ![](media/cb697e4930ae6a44e90b9af14aa0684e.png)  |
| 7 | Haben Sie sich erfolgreich verbunden, so wird das Symbol grün.                                                                                                                                                                                                                                                           | ![](media/f505e6f0f0ab4cc5b0578135b88e4dfe.png)  |

## 2.2. GNS3 Client

Führen Sie die nachfolgenden Schritte aus, um auf Ihrem Computer den GNS3 Client zu installieren.

![OpenVPN Installation Video](videos/GNS3_Installation.webm)

| N°| Schritt | Screenshot |
|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| 1 | Laden Sie auf *github* den GNS3 Installer herunter.  <https://github.com/GNS3/gns3-gui/releases>  **Wichtig: Nehmen Sie die Version 2.2.27 !**                               | ![](media/d7b2b1e2e87020f8ce82e54e01bcdc6b.png)                                                                           |
| 2 | Starten Sie den Installer und wählen Sie «Next».  Lesen die die Lizenzbestimmungen durch und klicken Sie danach «I Agree».  Im «Start Menu Folder»-Dialog wählen Sie «Next». | ![](media/ba7b378e70751c0ef2d4dcb9b0dea899.png) *Ignorieren Sie Versionsnummer in diesem und nachfolgenden Screenshots.*  |
| 3 | Wählen Sie die Komponenten gemäss Screenshot aus.   *Wenn Sie auf dem Server arbeiten, benötigen Sie keine lokale VM.  *                                                     | ![](media/2b71fbb7c4980344169ad43165e69695.png)![](media/5eea925e2cd5adad3736d61773b4d075.png)                            |
| 4 | Akzeptieren Sie die Standardeinstellungen und wählen Sie «Next».                                                                                                             | ![](media/fbd94b65f10c791b49e7bb818ebe40e3.png)                                                                           |
| 5 | Warten Sie, bis die Installation abgeschlossen ist.                                                                                                                          | ![](media/e616e42e2e25177cdc22b15944d49bb9.png)                                                                           |
| 6 | Wählen Sie «No» und klicken Sie auf «Next».                                                                                                                                  | ![](media/713478c99be498bfda4728299fe323a7.png)                                                                           |
| 7 | Wählen Sie «Start GNS3» ab (uncheck) und schliessen Sie die Installation mit «Finish» ab.                                                                                    | ![](media/4f2b307ca6134a5c1c795d734a591b00.png)                                                                           |


## 2.3. Verbindungstest

Stellen Sie mit einem **ping** sich sicher, dass Sie den Server erreichen können:

![](media/467b167e3b28df1a3bda6b9d6e07a24a.png)

## 2.4. GNS3 GUI

Bevor Sie die GNS3 GUI starten, stellen Sie sicher, dass Sie per OpenVPN mit dem Server verbunden sind.


Das nachfolgende Video ist ergänzend zur Anleitung und zeigt, wie nach der Installation ein GNS3 gestartet und ein Projekt gestartet werden kann. 

| 1 | Starten Sie GNS3.                                                                              | ![](media/89e2acfd0ba569dd9ad35b24719979c8.png) |
|---|------------------------------------------------------------------------------------------------|-------------------------------------------------|
| 2 | Wählen Sie *Edit* *Preferences* und dann *Server*                                              |                                                 |
| 3 | Füllen Sie die Felder wie im Screenshot aus und klicken Sie anschliessen auf «OK».             | ![](media/39b5634f949316c5248c9218f8131974.png) |
| 4 | Schliessen Sie GNS3 und öffnen Sie es erneut, damit die Einstellungen korrekt geladen werden.  |                                                 |
| 5 | Wenn alles erfolgreich war, sehen Sie jetzt das «Project»-Fenster.                             | ![](media/a2a3cdcd03eca9a2e4e014fd7836429a.png) |

**Wenn Sie sich erfolgreich mit dem Server verbunden haben, fahren Sie mit der [GNS3 Einführung](../20_GNS3%20Einführung/) fort.**

# 3. Hinweise

## 3.1. Mehrere Projekte auf dem gleichen Node

### 3.1.1. IP-Konflikte vermeiden

![Grafik mehrere Hosts](media/multipleprojects.PNG)

Der Zugang auf das GNS3 Labor erfolgt über eine Layer2-VPN Verbindung. Das [TAP-interface](https://de.wikipedia.org/wiki/TUN/TAP) ist mit br0 verbunden. Alle emulierten Geräte innerhalb der Projekte, die direkt oder über einen Switch mit br0 verbunden sind und eine Adresse im **.23** Subnetz haben, befinden sich im selben Layer2 Netz bzw. in der gleichen Broadcast-Domäne. 

Es gilt IP-Adress-Konflikte zu vermeiden. Die IP-Adressierung im **192.168.23.0/24** Subnetz muss über alle Projekte hinweg koordiniert werden. 

### 3.1.2. Unterschiedliche Projektnamen
Jeder Benutzer hat seine Projekte unterschiedlich zu bezeichnen. Zum Beispiel mit dem Anbringen eines persönliches Kürzels.

Beispiel
```
ALP 00 Ping mit zwei VPCS
```

**Wenn Sie ein neues Projekt erstellen, welches denselben Namen hat wie ein Bestehendes, wird dieses überschrieben.**