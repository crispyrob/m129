# Switching

Ein Switch ist ein Kopplungselement in Rechnernetzen, das Netzwerksegmente miteinander verbindet.

# BeASwitch
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie verstehen die grundlegende Funktion eines Switches. |

![Screenshot BeASwitch](media/BeASwitch.png)

## Vorgehen
 - Laden Sie sich auf https://github.com/muqiuq/BeASwitch/releases die Version *v1.4* der Applikation *BeASwitch* herunter.
 - Starten Sie das EXE (Anwendung ist nicht signiert und muss manuell vertraut werden!)<br>**Eventuell müssen Sie noch .NET Core 3.1 installieren.**
 - Spielen Sie die Anwendung so lange durch bis Sie keine Fehler mehr machen. 
 - Wenn Sie mindestens 30 von 30 richtig haben, gehen Sie zur Lehrperson. 

**Hinweis: Spielen Sie mit BeASwitch als ob es ein Rätsel wäre. Sie brauchen keine speziellen IT-Kenntnisse, um Herauszufinden wie die Anwendung funktioniert.**

# Praktische Übung - MAC-Tabelle

Eine praktische Übung zum Auslesen der MAC-Tabelle ist im Abschnitt [40_Wireshark](../40_Wireshark) zu finden. 