# Modulplanung - M129

# 1. Woche
 - Einführung ins neue Modul 129
 - Repetitionstest Modul 117
 - GNS3 Umgebung einrichten
 - GNS3 Ping Übung mit zwei PCs lösen
 - *Theorie:* 
   - Compendio Teil A (Zu lesen als Hausaufgabe nächste Woche)

# 2. Woche
 - Quiz zu Compendio Teil A
 - Einführung in Switching
 - BeASwitch

# 3. Woche
TBA

# 4. Woche
TBA

# 5. Woche
TBA

# 6. Woche
TBA