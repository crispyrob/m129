# GNS3 Labor Anforderungen  <!-- omit in toc -->

In diesem Dokument finden Sie alle GNS3 Labore zu den Kriterien der LB2.

# 1. Inhaltsverzeichnis
- [1. Inhaltsverzeichnis](#1-inhaltsverzeichnis)
- [2. Labor 1: Ping mit Switch](#2-labor-1-ping-mit-switch)
  - [2.1. Anforderungen](#21-anforderungen)
- [3. Labor 2: Ping mit Router](#3-labor-2-ping-mit-router)
  - [3.1. Anforderungen](#31-anforderungen)
  - [3.2. Alternative Variante](#32-alternative-variante)
- [4. Labor 3: Ping über Router (3 Subnetze) und lokalem PC](#4-labor-3-ping-über-router-3-subnetze-und-lokalem-pc)
  - [4.1. Anforderungen](#41-anforderungen)
- [5. Labor 4: GNS3 Labor 4: Aggregierte statische Routen](#5-labor-4-gns3-labor-4-aggregierte-statische-routen)
  - [5.1. Anforderungen](#51-anforderungen)
- [6. Labor 5: Labor mit zwei Router und DHCP Server](#6-labor-5-labor-mit-zwei-router-und-dhcp-server)
  - [6.1. Anforderungen](#61-anforderungen)
- [7. Labor 6: Labor mit zwei Router (NAT ins Internet), Debian Client](#7-labor-6-labor-mit-zwei-router-nat-ins-internet-debian-client)
  - [7.1. Anforderungen](#71-anforderungen)
    - [7.1.1. Variation (benötigt keinen MikroTik Account):](#711-variation-benötigt-keinen-mikrotik-account)
- [8. Labor 7: Erweiterung Labor 6 mit Firewall](#8-labor-7-erweiterung-labor-6-mit-firewall)
- [9. Labor 8: Ring mit 3 Routern, OSPF, DHCP und NAT ins](#9-labor-8-ring-mit-3-routern-ospf-dhcp-und-nat-ins)

# 2. Labor 1: Ping mit Switch
![Screenshot Labor1](media/Labor1.PNG)

## 2.1. Anforderungen
- PC1 und PC2 haben je eine statische IPv4-Adresse
- PC1 kann PC2 anpingen und umgekehrt
- Verwendetes Subnetz ist vermerkt

# 3. Labor 2: Ping mit Router
![Screenshot Labor1](media/Labor2.PNG)

## 3.1. Anforderungen
 - Zwei unterschiedliche /24 Subnetze
 - Router hat jeweils erste IPv4-Adresse
 - PC1 und PC2 haben jeweils eine statische IPv4-Adresse
 - PC1 kann PC2 anpingen und umgekehrt

## 3.2. Alternative Variante



# 4. Labor 3: Ping über Router (3 Subnetze) und lokalem PC
![Screenshot Labor1](media/Labor3.PNG)

## 4.1. Anforderungen
- IPv4 Subnetz A ist wie folgt festgelegt:
   - 192.168.X.0/24
   - X = 10 + Position in Klassenliste
   - Bsp. X = 10 + 1 => 11 => 192.168.11.0/24
 - IPv4 Subnetz B wird wie Netz A festgelegt
   - X = 11 + Position in Klassenliste
   - Bsp. X = 20 + 1 => 21 => 192.168.21.0/24
 - PC2 kann PC1 anpingen und umgekehrt
 - PC1 und PC2 sind vom eigenen Laptop/PC anpingbar.
 - Netz C hat eine Maske /30
 - Alle Subnetze sind klar vermerkt und Abgrenzungen grafisch erkennbar

# 5. Labor 4: GNS3 Labor 4: Aggregierte statische Routen
![Screenshot Labor1](media/Labor4.PNG)

## 5.1. Anforderungen
 - IPv4 Subnetz A ist wie folgt festgelegt:
   - 192.168.X.0/24
   - X = 10 + Position in Klassenliste
   - Bsp. X = 10 + 1 => 11 => 192.168.11.0/24
 - Subnetz B: 192.168.X.0/24 X = 128 + Pos in KL-Liste
 - Subnetz C: 192.168.X.0/24 X = 192 + Pos in KL-Liste
 - Alle VPCS könne alle anderen anpingen
 - Alle VPCS sind vom eigenen Laptop/PC anpingbar.
 - In jedem Netz hat es ein VPCS mit einer Host Adresse aus dem entsprechenden Subnetz.
 - Netz C,E haben eine Maske /30
 - Auf R1 ist für Netz B und D nur eine Route eingetragen
 - Auf R1 ist keine Default Route eingetragen
 - Auf dem eigenen Laptop/PC sind nur zwei zusätzliche Routen eingetragen
 - Alle Subnetze sind klar vermerkt und Abgrenzungen sind grafisch erkennbar


# 6. Labor 5: Labor mit zwei Router und DHCP Server
![Screenshot Labor1](media/Labor5.PNG)

## 6.1. Anforderungen
Anforderungen:
 - Mind 2 /24 Subnetze (Netz A und Netz B)
 - Netz A: 192.168.X.0/24
   - X = 32 + Position in Klassenliste
 - Netz B: 192.168.X.0/24
   - X = 33 + Position in Klassenliste
 - Jeweils ein DHCP Server für Subnetze *A* und *B*
 - PC1 bis PC4 erhalten IP Adresse via DHCP
 - Jeder PC kann alle anderen PCs und Router anpingen
 - Router haben immer die erste Host-Adresse (Host-min)
 - Kommunikation zwischen R1 und R2 erfolgt via Subnetz *C*
 - Subnetz C muss Maske /30 haben.
 - R1 und R2 sind via Laptop des Lernenden erreichbar.
 - Eine Firewallregel verhindert, dass PC1 und PC2 vom Subnetz 192.168.23.0/24 angepingt werden können. 
 - Auf die Webinterfaces von R1 und R2 kann vom eigenen Laptop/PC zugegriffen werden


# 7. Labor 6: Labor mit zwei Router (NAT ins Internet), Debian Client
![Screenshot Labor1](media/Labor6.PNG)

## 7.1. Anforderungen
 - Auf PC1 ist lxde installiert
 - Surfen im Internet (mit Webbrowser) ist möglich.
 - Alle ausgehenden IP Pakete auf R1-ether2 werden "masqueraded" (NAPT)
 - Trial Lizenzen auf R1 und R2 sind aktiviert (MikroTik Account benötigt)
 - Netz A: Beliebiges IPv4 Subnet
 - Netz B: 192.168.X.0/24
   - X = 32 + Position in Klassenliste

### 7.1.1. Variation (benötigt keinen MikroTik Account):
 - Als R1 OPNsense oder pfSense einsetzen
 - Als R2 cisco Router einsetzen

* Windows PC ist optional

# 8. Labor 7: Erweiterung Labor 6 mit Firewall
*To be defined*

# 9. Labor 8: Ring mit 3 Routern, OSPF, DHCP und NAT ins
*To be defined*