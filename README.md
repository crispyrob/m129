# M129 - LAN-Komponenten in Betrieb nehmen 

Eine Übersicht über die im Modul 129 zu erreichenden Kompetenzen finden Sie im Abschnitt [Kompetenzmatrix](01_Kompetenzmatrix/)

# Leistungsbeurteilung
Alle Informationen zu der Leistungsbeurteilung finden Sie [hier](/00_evaluation/README.md). 

# Unterlagen
  - Gitrepository [ch-tbz-it/Stud/m129](https://gitlab.com/ch-tbz-it/Stud/m129)
  - Gitbook [ch-tbz-it.gitlab.io/Stud/m129/](https://ch-tbz-it.gitlab.io/Stud/m129/)
 - Unterlagen die aus urheberrechtlichen Gründen nicht hier veröffentlicht werden können, sind im MS Teams zu finden. 

# Weiterführende Links
 - Dokumentation in Markdown mit GitLab: https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/02_git
 - BeASwitch / BeARouter: https://github.com/muqiuq/BeASwitch
 - LernMAAS der TBZ https://github.com/mc-b/lernmaas
 - Unterlagen zum Modul 129 von Jürg Arnold: https://edu.juergarnold.ch/modul129.html
 - IPv4 Calculator http://jodies.de/ipcalc
 - Visual Subnet Calculator https://www.davidc.net/sites/default/subnets/subnets.html