# M129 Theoretische Grundlagen  <!-- omit in toc -->

# 1. Datennetzwerk - Netzwerksymbole - OSI-Modell
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  2 Lektion |
| Ziele | Notwendige theoretische Grundlagen und Begriffe für das Modul 129 erarbeiten.  |

Im Teil A des Lehrmittels von *Compendio* vermittelt die theoretischen Grundlagen und die Begriffe für das Modul 129.    
 - Was ist ein Datennetzwerk?
 - Die Komponenten eines Netzwerkes
 - Aufbau eines Datennetzwerkes
 - Netzwerksymbole (*Prüfungsrelevant)
 - OSI-Modell
 - Unterschied TCP/IP Stack <=> OSI Modell (*Prüfungsrelevant)

Die PDFs werden Ihnen über einen separaten Kanal (z.B. *MS Teams* zur Verfügung gestellt.)
 - M129-Compendio-A00-Teil A - Netzwerk Grundlagen.pdf
 - M129-Compendio-A01-Was sind Datennetzwerke.pdf
 - M129-Compendio-A02-Wie funktionieren Datennetzwerke.pdf

**Tipp:** An der LB1 dürfen Sie eine selbstgeschriebene Zusammenfassung mitbringen. Erstellen Sie die Zusammenfassung gleich während der Bearbeitung der Lektüre. 

# 2. Ethernet - Data Link Layer
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  2 Lektion |
| Ziele | Notwendige theoretische Grundlagen und Begriffe für das Modul 129 erarbeiten.  |

Im ersten Teil des Teil B des Lehrmittels von *Compendio* vermittelt die Grundlagen zum Thema Ethernet / Switching.    
 - Ethernet-Technologien auswählen
 - Topologien, Zugriffsverfahren und Verkabelung
 - Netzwerkgeräte auswählen und verbinden
 - Ethernet-Switches auswählen und verbinden

Die PDFs werden Ihnen über einen separaten Kanal (z.B. *MS Teams* zur Verfügung gestellt.)
 - M129-Compendio-B00-Teil B - LAN aufbauen und testen.pdf
 - M129-Compendio-B03-Ethernet-Technologien auswählen.pdf
 - M129-Compendio-B04-Netzwerkgeräte auswählen und verbinden.pdf
