# Kompetenzmatrix - Modul 129

## Handlungsziele und typische Handlungssituationen
##### 1. Anforderungen für ein neues Netzwerk aufnehmen und die erforderlichen Netzwerkkomponenten bestimmen (Switch, Router). 
Der Lernende Kim diskutiert Raumeinteilung, Stockwerkverteilung, Arbeitsplatzanordnung und vorhandene Dienste mit dem Kunden für den neuen Firmenstandort und erfasst alle notwendigen Informationen, damit die weitere Planung zügig erledigt werden kann. Er bestimmt erforderliche Netzwerkkomponenten, um das Netzwerk zukunftssicher und fachgerecht aufbauen zu können.

##### 2. Adressschema für IP Netz mit Subnetzen anpassen und geeignetes Subnetting mit zugehöriger Netzmaske aus Vorgaben ableiten (z. B. Aufteilung in IP Netze, Anzahl Clients).  
Aufgrund der Vorgaben bestimmt Kim sinnvolle und fachgerechte Subnetzgrössen und berücksichtigt bei der Netztrennung Last- und Sicherheitsaspekte. Er reflektiert seine Lösung mit den Netzwerkspezialisten.

##### 3. Netzwerkkomponenten gemäss Netzwerkschema und Adressierung in Betrieb nehmen und konfigurieren. 

Die aktiven Netzwerkkomponenten werden von Kim vorbereitet, konfiguriert und getestet. Danach werden die Geräte eingebaut und wo nötig Einstellungen auf den Client-Geräten angepasst.

##### 4. Statisches Routing gemäss Netzwerkschema implementieren und Routing Tabelle interpretieren. 

Damit die Funktionalität gewährleistet ist, analysiert Kim die bereits vorhandenen Routingtabellen des alten Standortes. Er passt diese auf die neuen Gegebenheiten (Standort, Funktionswünsche) an, implementiert diese in den Routern und testet das Routing.

##### 5. Konfigurationsfehler und ihre Ursachen mit geeigneten Hilfsprogrammen analysieren und beheben. 

Nach Inbetriebnahme am Standort treten Fehler auf, die Kim mit gut strukturiertem Vorgehen und geeigneten Tools findet und in der Konfiguration beheben kann.

##### 6. Netzwerkdokumentation erstellen bzw. nachführen (Konfiguration, Netzwerkschema). 

Die bestehende Netzwerkdokumentation wird von Kim an die neuen Begebenheiten angepasst und die Konfiguration aller aktiven Komponenten fachgerecht festgehalten.

##### 7. Netzwerk mit einem Abnahmeprotokoll dem Kunden übergeben. 

Dem Kunden kann Kim die Anlage fristgerecht übergeben, ein Abnahmeprotokoll bestätigt die Funktion und entlastet ihn.

## Matrix

| Kompetenzband: | HZ | Grundlagen | Fortgeschritten | Erweitert |
| --------------------- | --------------------------- | ------------------ | ------------------ | ------------------ |
| Grundlagen und Standard        | 1    | A1G: Ich kann wesentliche Technologien und Standards zuordnen und ein Netzwerkschema interpretieren.. | A1F: Ich kann Technologien (im Bereich LAN, WLAN) und Standards (ISO/OSI, Protokolle) zuordnen und erklären, sowie Details aus einem Netzwerkschema entnehmen. | A1E:  Ich kann Technologien (im Bereich LAN, WLAN) und Standards (ISO/OSI, Protokolle) , erklären und deren Wirkung in Praxisanwendungen zuordnen. Die Angaben eines Netzwerkschema kann ich erklären. |
| Funktion und Konfiguration     | 1, 3 | B1G: Ich kann die Funktionsweise von Switches und Routern erklären, sowie diese in Betrieb nehmen. | B1F: Ich kann wesentliche Eigenschaften eines Switches aufzeigen, die Funktionsweise eines Routers erklären und diese Geräte fachgerecht konfigurieren und in Betrieb nehmen. Zudem kann ich wichtige Sicherheitseinstellungen (Default Password, Grundlagen) vornehmen. | B1E:  Ich kann die Eigenschaften eines Switches (inkl. managable, stackable, auto-sense, spanning tree, mirroring und bridging) aufzeigen, die Funktionsweise eines Routers erklären und diese Geräte fachgerecht konfigurieren und in Betrieb nehmen. Zudem kann ich wichtige Sicherheitseinstellungen (Password, Protokolle ausschalten, Logging) einplanen und umsetzen. |
| Adressierung und Subnettierung | 2    | C1G: Ich kann in einem Subnetz eine fachgerechtes IP-Adresskonzept für verschiedene Gerätegruppen erstellen. | C1F: Ich kann die IP-Adressierung für mehrere Subnetze in einem LAN fachgerecht planen und umsetzen. | C1E: Ich kann die IP-Adressierung für mehrere Subnetze in einem LAN fachgerecht planen und umsetzen. Ich sehe dabei spezielle Bereiche für Dienste, Geräte (statische IP-Adressierung), sowie Spezialanwendungen vor und zeige zukünftige Erweiterungsmöglichkeiten auf. |
| Routing                        | 4    | D1G: Ich kann einfache statische Routingtabellen umsetzen. | D1F: Ich kann die Vor- und Nachteile von statischen/dynamischen Routing erklären und statische Routingtabellen für ein LAN mit mehreren Subnetzen fachgerecht planen und umsetzen. | D1E: Ich kann die Vor- und Nachteile von statischen/dynamischen Routing erklären und statische Routingtabellen für ein LAN mit mehreren Subnetzen fachgerecht planen und umsetzen. Ich kann erklären, wie Spezialfälle oder Erweiterungen umgesetzt werden, bzw. wo Schwachstellen auftreten können. |
| Fehleranalyse und -behebung    | 5    | E1G: Ich kann grundlegende Befehle zur Fehlersuche anwenden. | E1F: Ich kenne verschiedene Werkzeuge zur Analyse der Funktion des Netzwerkes und kann damit Fehlfunktionen eingrenzen und beheben. | E1E: Ich kenne verschiedene Werkzeuge zur Analyse der Funktion des Netzwerkes und kann damit Fehlfunktionen systematisch (Ausschlussverfahren, OSI-Schichtenmodell), und effizient (zielgerichteter Einsatz) eingrenzen und beheben, sowie die Dienstgüte (Bandbreitenmanagement) beurteilen. |
| Netzwerkdokumentation          | 6    | F1G: Ich kann ein einfaches Netzwerkschema erstellen und wesentliche Aspekte dazu dokumentieren. | F1F: Ich kann ein vollständiges Netzwerkschema erstellen, sowie Konfigurationen und wesentliche Aspekte nachvollziehbar dokumentieren. | F1E: Ich kann ein vollständiges Netzwerkschema erstellen, sowie Konfigurationen und wesentliche Aspekte nachvollziehbar dokumentieren. In der Dokumentation sind wichtige Überlegungen festgehalten, damit Ergänzungen oder ein Neuaufbau effizient bewerkstelligt werden kann. |
| Test- und Abnahmeprotokoll     | 7    | G1G: Ich kann mit einfachen Tests die Funktion eines Netzwerkes prüfen und dies mit einem Protokoll belegen. | G1F: Ich kann mit gut gewählten Testfällen die Funktion eines Netzwerkes prüfen und notwendige Massnahmen umsetzen, sowie ein angemessenes Abnahmeprotokoll erstellten. | G1E: Ich kann mit gut gewählten Testfällen die Funktion und Grenzen (Lasttest, spez. Fälle) eines Netzwerkes prüfen und notwendige Massnahmen umsetzen, sowie ein angemessenes Abnahmeprotokoll erstellten. |



## Kompetenzstufen

### Grundlagen | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.0.*

### Fortgeschritten | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.5*

### Erweitert | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*

## Fragekatalog

Link zum [Fragekatalog](Fragenkatalog.md)
